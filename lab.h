#ifndef LAB_H
#define LAB_H


typedef struct{
	char NombreCarrera[91];
	int Codigo[5];
	int NEM[2];
	int Ranking[2];
	int Lenguaje[2];
	int Matematica[2];
	int Historia[2];
	int Ciencia[2];
	int Ponderacion[3];
	int PSU[3];
	float Maximo[6];
	float Minimo[6];
	int CupoPSU[3];
	int BEA[2];
} carrera;


void cargarAdmision(char path[], carrera universidad[]); 




#endif
