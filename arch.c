#include "lab.h"
#include <stdio.h>
#include <stdlib.h>

void cargarAdmision(char path[], carrera universidad[]){ //Se define la funcion cargarAdmision
	FILE *file; //Llama al archivo
	int i = 0;
	if((file=fopen(path,"r"))==NULL){ //Abre el archivo y lo lee, para luego preguntar si el archivo se encuentra vacio
		printf("\n Error al arbir el archivo");
		exit(0); 
	}
	else{
		printf("\n *** Admisión 2018 Universidad de Valparaíso Chile ***\n");
		while (i != 53) {
			fscanf(file,"%c %d %d %d %d %d %d %d %d %d %f %f %d %d", universidad[i].NombreCarrera,universidad[i].Codigo,universidad[i].NEM,universidad[i].Ranking,universidad[i].Lenguaje,universidad[i].Matematica,universidad[i].Historia,universidad[i].Ciencia,universidad[i].Ponderacion,universidad[i].PSU,universidad[i].Maximo,universidad[i].Minimo,universidad[i].CupoPSU,universidad[i].BEA); //Guarde los datos en diferentes variables
		}
		fclose(file); //Cierra el archivo
	} 
}